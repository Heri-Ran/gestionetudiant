package gestionetudiant.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Etudiant {
	private int idEtudiant;
	private String nom;
	private String prenom;
	private int age;
	private String adresse;
	private String genre;
	public Etudiant(int idEtudiant, String nom, String prenom, int age, String adresse, String genre) {
		super();
		this.idEtudiant = idEtudiant;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.adresse = adresse;
		this.genre = genre;
	}
}
